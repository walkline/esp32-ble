from machine import Pin, PWM, Timer
from utime import sleep_ms

BRIGHTNESS_NONE = const(0)
BRIGHTNESS_LOW = const(10)
BRIGHTNESS_MEDIUM = const(50)
BRIGHTNESS_HIGH = const(200)

# ACTION_NORMAL = const(-1)
ACTION_ON = const(0)
ACTION_OFF = const(1)


class  LedPadException(Exception):
	pass


class LedBase(object):
	def __init__(self, io):
		self.__pin = Pin(io, Pin.OUT, Pin.PULL_UP, value=1)
		self.__brightness = BRIGHTNESS_MEDIUM
	
	def deinit(self):
		self.__pin = None

	def set_brightness(self, level):
		self.__brightness = level
	
	def on(self):
		self.__pin.on()
	
	def off(self):
		self.__pin.off()


class LedPad(object):
	"""
	5*4 LED 扫描矩阵驱动
	"""
	def __init__(self, pin_set=None):
		assert pin_set is not None and isinstance(pin_set, tuple) and len(pin_set) == 2, LedPadException("pin_set must be a tuple, e.g. ((row output io), (column input io))")

		self.__row_io_set = []
		self.__column_io_set = []

		try:
			for io in pin_set[0]:
				self.__row_io_set.append(LedBase(io))
			
			for io in pin_set[1]:
				self.__column_io_set.append(LedBase(io))
		except IndexError:
			raise LedPadException("pin_set value error")

		self.__row_count = len(self.__row_io_set)
		self.__column_count = len(self.__column_io_set)

		assert self.__row_count > 0 and self.__column_count > 0, LedPadException("pin_set value error")

		self.__key_map = [
			[0, 0, 0],
			[0, 0, 0],
			[0, 0, 0],
			[0, 0, 0],
			[0, 0, 0]
		]

	def deinit(self):
		for io in self.__row_io_set:
			io.deinit()
		for io in self.__column_io_set:
			io.deinit()
		
		self.__row_io_set = None
		self.__column_io_set = None

	def get_led_count(self):
		"""
		获取 Led 数量
		"""
		return self.__row_count * self.__column_count

	def run_test(self):
		pic = [
			[1, 0, 1],
			[0, 1, 0],
			[1, 0, 1],
			[1, 0, 1],
			[0, 1, 0]
		]

		while True:
			for index_row in range(self.__row_count):
				self.__row_io_set[index_row].on()
				for index_col in range(self.__column_count):
					if pic[index_row][index_col]:
						self.__column_io_set[index_col].off()
					self.__column_io_set[index_col].on()
				self.__row_io_set[index_row].off()

	def run(self):
		while True:
			for index_row in range(self.__row_count):
				self.__row_io_set[index_row].on()
				for index_col in range(self.__column_count):
					if self.__key_map[index_row][index_col]:
						self.__column_io_set[index_col].off()
					self.__column_io_set[index_col].on()
				self.__row_io_set[index_row].off()
				
	def light_on(self, row, column):
		self.__key_map[row][column] = 1

	def light_off(self, row, column):
		self.__key_map[row][column] = 0


def main():
	ROW_SET = (13, 12, 14, 27, 26) # for output
	COLUMN_SET = (25, 33, 32) # for intput
	PIN_SET = (ROW_SET, COLUMN_SET)

	ledpad = LedPad(PIN_SET)
	print("LedPad led count:", ledpad.get_led_count())

	ledpad.light_on(0, 0)
	ledpad.light_on(3, 2)
	ledpad.run()
	
	# ledpad.run_test()

	# ledpad.deinit()
	# ledpad = None


if __name__ == "__main__":
	try:
		main()
	except KeyboardInterrupt:
		print("\nPRESS CTRL+D TO RESET DEVICE")
