"""
The MIT License (MIT)
Copyright © 2020 Walkline Wang (https://walkline.wang)
https://gitee.com/walkline/esp32-ble
"""
from keyboard.keycode import KeyCode


class KeycodeTables(object):
	MOUSE_REPORT_ID = 1
	KEYBOARD_REPORT_ID = 2

	# http://www.freebsddiary.org/APC/usb_hid_usages
	REPORT_MAP_DATA = [
	    0x05, 0x01, # Usage Page (Generic Desktop)
		0x09, 0x02, # Usage (Mouse)
		0xA1, 0x01, # Collection (Application)
		0x85, 0x01, #	Report Id (1)
		0x09, 0x01, #	Usage (Pointer)
		0xA1, 0x00, #	Collection (Physical)
		0x05, 0x09, #		Usage Page (Buttons)
		0x19, 0x01, #		Usage Minimum (01) - Button 1
		0x29, 0x03, #		Usage Maximum (03) - Button 3
		0x15, 0x00, #		Logical Minimum (0)
		0x25, 0x01, #		Logical Maximum (1)
		0x75, 0x01, #		Report Size (1)
		0x95, 0x03, #		Report Count (3)
		0x81, 0x02, #		Input (Data, Variable, Absolute) - Button states
		0x75, 0x05, #		Report Size (5)
		0x95, 0x01, #		Report Count (1)
		0x81, 0x01, #		Input (Constant) - Padding or Reserved bits
		0x05, 0x01, #		Usage Page (Generic Desktop)
		0x09, 0x30, #		Usage (X)
		0x09, 0x31, #		Usage (Y)
		0x09, 0x38, #		Usage (Wheel)
		0x15, 0x81, #		Logical Minimum (-127)
		0x25, 0x7F, #		Logical Maximum (127)
		0x75, 0x08, #		Report Size (8)
		0x95, 0x03, #		Report Count (3)
		0x81, 0x06, #		Input (Data, Variable, Relative) - X & Y coordinate
		0xC0,       #	End Collection
		0xC0,       # End Collection

		0x05, 0x01, # USAGE_PAGE (Generic Desktop)
		0x09, 0x06, # USAGE (Keyboard)
		0xA1, 0x01, # COLLECTION (Application)

		# 8 keycode 控制键键位
		0x85, 0x02, #	Report Id (2)
		0x05, 0x07, #	USAGE_PAGE (Keyboard)
		0x19, 0xE0, # 	USAGE_MINIMUM (Keyboard LeftControl)
		0x29, 0xE7, # 	USAGE_MAXIMUM (Keyboard Right GUI)
		# keycode value 0 and 1
		0x15, 0x00, # 	LOGICAL_MINIMUM (0)
		0x25, 0x01, # 	LOGICAL_MAXIMUM (1)
		# Modifier byte
		0x75, 0x01, # 	REPORT_SIZE (1)
		0x95, 0x08, # 	REPORT_COUNT (8)
		0x81, 0x02, # 	INPUT (Data,Var,Abs)

		# Reserved byte 保留键位
		0x95, 0x01, # 	REPORT_COUNT (1)
		0x75, 0x08, # 	REPORT_SIZE (8)
		0x81, 0x01, # 	INPUT (Cnst,Var,Abs)

		# Key arrays (6 bytes) 常规键键位
		0x05, 0x07, # 	USAGE_PAGE (Keyboard)
		0x19, 0x00, # 	USAGE_MINIMUM (Reserved (no event indicated))
		0x29, 0xA4, # 	USAGE_MAXIMUM (Keyboard ExSel)
		0x15, 0x00, # 	LOGICAL_MINIMUM (0)
		0x25, 0xA4, # 	LOGICAL_MAXIMUM (164)
		0x95, 0x06, # 	REPORT_COUNT (6)
		0x75, 0x08, # 	REPORT_SIZE (8)
		0x81, 0x00, # 	INPUT (Data,Ary,Abs)

		# # LED report 不要被动灯光控制
		# 0x05, 0x08, # 	USAGE_PAGE (LEDs)
		# 0x19, 0x01, # 	USAGE_MINIMUM (Num Lock)
		# 0x29, 0x05, # 	USAGE_MAXIMUM (Kana)
		# 0x95, 0x05, # 	REPORT_COUNT (5)
		# 0x75, 0x01, # 	REPORT_SIZE (1)
		# 0x91, 0x02, # 	OUTPUT (Data,Var,Abs)

		# # LED report padding 补位
		# 0x95, 0x01, # 	REPORT_COUNT (1)
		# 0x75, 0x03, # 	REPORT_SIZE (3)
		# 0x91, 0x03, # 	OUTPUT (Cnst,Var,Abs)
		0xC0,		# END_COLLECTION
	]

	# 键盘层定义
	KEYPAD_LAYERS = [
		# layer1: 数字键盘，带方向键
		[
			(KeyCode.KCC_NUM, KeyCode.KCC_NONE, KeyCode.KCC_NONE, KeyCode.KCC_NONE, KeyCode.KCC_SET),
			((KeyCode.KC_7, KeyCode.KC_F1), (KeyCode.KC_8, KeyCode.KC_F2), (KeyCode.KC_9, KeyCode.KC_F3), (KeyCode.KC_DEL, KeyCode.KC_F4), (KeyCode.KC_F2, KeyCode.KC_PGUP)),
			((KeyCode.KC_4, KeyCode.KC_F5), (KeyCode.KC_5, KeyCode.KC_F6), (KeyCode.KC_6, KeyCode.KC_F7), (KeyCode.KC_PEQL, KeyCode.KC_F8), (KeyCode.KC_PPLS, KeyCode.KC_PGUP)),
			((KeyCode.KC_1, KeyCode.KC_F9), (KeyCode.KC_2, KeyCode.KC_F10), (KeyCode.KC_3, KeyCode.KC_F11), (KeyCode.KC_UP, KeyCode.KC_F12), (KeyCode.KC_ENTER, KeyCode.KC_HOME)),
			((KeyCode.KC_0, KeyCode.KC_ESC), (KeyCode.KCC_FN, KeyCode.KCC_NONE), (KeyCode.KC_LEFT, KeyCode.KC_PCMM), (KeyCode.KC_DOWN, KeyCode.KC_DOT), (KeyCode.KC_RIGHT, KeyCode.KC_END)),
		],
		# layer2：功能键，带 Esc、Win、Vol+和Vol-
		[
			(KeyCode.KCC_NUM, KeyCode.KCC_NONE, KeyCode.KCC_NONE, KeyCode.KCC_NONE, KeyCode.KCC_SET),
			(),
			(),
			(),
			(),
		],
		# layer3: 组合键测试
		[
			(KeyCode.KCC_NUM, KeyCode.KCC_NONE, KeyCode.KCC_NONE, KeyCode.KCC_NONE, KeyCode.KCC_SET),
			(),
			(),
			(),
			(),
		],
		# layer4
		[
			(KeyCode.KCC_NUM, KeyCode.KCC_NONE, KeyCode.KCC_NONE, KeyCode.KCC_NONE, KeyCode.KCC_SET),
			(KeyCode.KCC_TEST, KeyCode.KCC_NONE, KeyCode.KCC_NONE, KeyCode.KCC_NONE, KeyCode.KCC_LIGHT),
			(KeyCode.KCC_TEST, KeyCode.KCC_NONE, KeyCode.KCC_NONE, KeyCode.KCC_NONE, KeyCode.KCC_BUP),
			(KeyCode.KCC_TEST, KeyCode.KCC_NONE, KeyCode.KCC_NONE, KeyCode.KCC_NONE, KeyCode.KCC_BDOWN),
			(KeyCode.KCC_TEST, KeyCode.KCC_NONE, KeyCode.KCC_NONE, KeyCode.KCC_NONE, KeyCode.KCC_NONE),
		],
	]
